import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelecaoespComponent } from './selecaoesp.component';

describe('SelecaoespComponent', () => {
  let component: SelecaoespComponent;
  let fixture: ComponentFixture<SelecaoespComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelecaoespComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelecaoespComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
