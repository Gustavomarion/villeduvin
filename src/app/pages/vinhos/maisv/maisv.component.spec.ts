import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaisvComponent } from './maisv.component';

describe('MaisvComponent', () => {
  let component: MaisvComponent;
  let fixture: ComponentFixture<MaisvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaisvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaisvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
