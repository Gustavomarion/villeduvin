import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposvComponent } from './tiposv.component';

describe('TiposvComponent', () => {
  let component: TiposvComponent;
  let fixture: ComponentFixture<TiposvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiposvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiposvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
