import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alphaville',
  templateUrl: './alphaville.component.html',
  styleUrls: ['./alphaville.component.scss', '../lojas.component.scss']
})
export class AlphavilleComponent implements OnInit {

  public unidadeAlpha: any[] = [
    {
      id: 1,
      img: './assets/photos/alphaville.png'
    },
    {
      id: 2,
      img: './assets/photos/alphaville.png'
    }
  ];

  public activeUnidadeAlpha: any  = {};  // atributo

        unidadeAlphaConfig = {  // atributo
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
    };

    constructor() { }
    // metodos abaixo do construtor


    public Slide(slide: any) {  // metodo
        console.log(slide);
        this.activeUnidadeAlpha = slide;
    }

    ngOnInit() {  // metodo
        this.activeUnidadeAlpha = this.unidadeAlpha[0];
    }


    teste() {
        console.log('teste');
    }

    slickInit(e) {
        console.log('slick initialized');
    }

    breakpoint(e) {
        console.log('breakpoint');
    }

    afterChange(e) {
        this.activeUnidadeAlpha = this.unidadeAlpha[e.currentSlide];
    }

    beforeChange(e) {
    }

}
