import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlphavilleComponent } from './alphaville.component';

describe('AlphavilleComponent', () => {
  let component: AlphavilleComponent;
  let fixture: ComponentFixture<AlphavilleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlphavilleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlphavilleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
