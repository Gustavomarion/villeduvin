import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-itaim',
  templateUrl: './itaim.component.html',
  styleUrls: ['./itaim.component.scss', '../lojas.component.scss']
})
export class ItaimComponent implements OnInit {

  public unidadeItaim: any[] = [
    {
      id: 1,
      img: './assets/photos/itaim.png'
    },
    {
      id: 2,
      img: './assets/photos/itaim.png'
    }
  ];

    public activeUnidadeItaim: any  = {};  // atributo

        unidadeItaimConfig = {  // atributo
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        autoplay: true,
    };

    // atributos acima do construtor
    constructor() { }
    // metodos abaixo do construtor

    public Slide(slide: any) {  // metodo
      console.log(slide);
      this.activeUnidadeItaim = slide;
  }

    ngOnInit() {
        this.activeUnidadeItaim = this.unidadeItaim[0];
    }


    teste() {
        console.log('teste');
    }

    slickInit(e) {
        console.log('slick initialized');
    }

    breakpoint(e) {
        console.log('breakpoint');
    }

    afterChange(e) {
        this.activeUnidadeItaim = this.unidadeItaim[e.currentSlide];
    }

    beforeChange(e) {
    }

}
