import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItaimComponent } from './itaim.component';

describe('ItaimComponent', () => {
  let component: ItaimComponent;
  let fixture: ComponentFixture<ItaimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItaimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
