import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-corporativos',
  templateUrl: './corporativos.component.html',
  styleUrls: ['./corporativos.component.scss']
})
export class CorporativosComponent implements OnInit {

  constructor() { }

  public espacos: any[] = [
    {
      id: 1,
      img: './assets/photos/alphaville.png'
    },
    {
      id: 2,
      img: './assets/photos/itaim.png'
    }
  ];

  public menu: any[] = [
    {
      id: 1,
      img: './assets/photos/img-um.png',
      name: 'Nome do Prato'
    },
    {
      id: 2,
      img: './assets/photos/img-dois.png',
      name: 'Nome do Prato'
    },
    {
      id: 3,
      img: './assets/photos/img-tres.png',
      name: 'Nome do Prato'
    },
    {
      id: 4,
      img: './assets/photos/img-quatro.png',
      name: 'Nome do Prato'
    },
    {
      id: 5,
      img: './assets/photos/img-cinco.png',
      name: 'Nome do Prato'
    },
    {
      id: 6,
      img: './assets/photos/img-seis.png',
      name: 'Nome do Prato'
    },
    {
      id: 7,
      img: './assets/photos/img-tres.png',
      name: 'Nome do Prato'
    }
  ];

  public activeMenu: any = {};

  menuConfig = {
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
  };

  public activeMenuMob: any = {};
  menuMobConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
  };

  public activeEspacos: any = {};

  espacosConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
  };

  public SlideMenu(slide: any) {
    console.log(slide);
    this.activeMenu = slide;
  }

  public SlideMenuMob(slide: any) {
    console.log(slide);
    this.activeMenuMob = slide;
  }

  public Slide(slide: any) {
    console.log(slide);
    this.activeEspacos = slide;
  }

  ngOnInit() {
    this.activeEspacos = this.espacos[0];
    this.activeMenu = this.menu[0];
    this.activeMenuMob = this.menu[0];
  }


  teste() {
    console.log('teste');
  }

  slickInit(e) {
    console.log('slick initialized');
  }

  breakpoint(e) {
    console.log('breakpoint');
  }

  afterChange(e) {
    this.activeEspacos = this.espacos[e.currentSlide];
    this.activeMenu = this.menu[e.currentSlide];
    this.activeMenuMob = this.menu[e.currentSlide];
  }

  beforeChange(e) {
  }

}
