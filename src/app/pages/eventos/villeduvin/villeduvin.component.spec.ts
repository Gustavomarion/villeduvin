import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VilleduvinComponent } from './villeduvin.component';

describe('VilleduvinComponent', () => {
  let component: VilleduvinComponent;
  let fixture: ComponentFixture<VilleduvinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VilleduvinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VilleduvinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
