import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-porpreco',
  templateUrl: './porpreco.component.html',
  styleUrls: ['./porpreco.component.scss', '../home.component.scss']
})
export class PorprecoComponent implements OnInit {

  public vinhos: any[] = [
    {
      id: 1,
      name: 'Vinho Crasto Douro',
      price: 'R$ 194',
      wine: './assets/photos/vinho.png',
      flag: './assets/icons/bandeira.png',
      pais: 'Portugal',
      grapes: 'Tinta Roriz, Touriga Franca,Touriga Nacional e Tinta Barroca.',
      produtor: 'Quinta do Crasto',
    },
    {
      id: 2,
      name: 'Vinho Crasto Douro',
      price: 'R$ 193',
      wine: './assets/photos/vinho.png',
      flag: './assets/icons/bandeira.png',
      pais: 'Portugal',
      grapes: 'Tinta Roriz, Touriga Franca,Touriga Nacional e Tinta Barroca.',
      produtor: 'Quinta do Crasto',
    },
    {
      id: 3,
      name: 'Vinho Crasto Douro',
      price: 'R$ 192',
      wine: './assets/photos/vinho.png',
      flag: './assets/icons/bandeira.png',
      pais: 'Portugal',
      grapes: 'Tinta Roriz, Touriga Franca,Touriga Nacional e Tinta Barroca.',
      produtor: 'Quinta do Crasto',
    },
    {
      id: 4,
      name: 'Vinho Crasto Douro',
      price: 'R$ 191',
      wine: './assets/photos/vinho.png',
      flag: './assets/icons/bandeira.png',
      pais: 'Portugal',
      grapes: 'Tinta Roriz, Touriga Franca,Touriga Nacional e Tinta Barroca.',
      produtor: 'Quinta do Crasto',
    }
  ];

  public activeVinhos: any  = {};  // atributo

    vinhosConfig = {  // atributo
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
    };

    vinhoConfig = {  // atributo
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
  };
    
    // atributos acima do construtor
    constructor() { }
    // metodos abaixo do construtor


    public Slide(slide: any) {  // metodo
        console.log(slide);
        this.activeVinhos = slide;
    }


    ngOnInit() {  // metodo
        this.activeVinhos = this.vinhos[0];
    }


    teste() {
        console.log('teste');
    }

    slickInit(e) {
        console.log('slick initialized');
    }

    breakpoint(e) {
        console.log('breakpoint');
    }

    afterChange(e) {
        this.activeVinhos = this.vinhos[e.currentSlide];
    }

    beforeChange(e) {
    }

}
