import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PorprecoComponent } from './porpreco.component';

describe('PorprecoComponent', () => {
  let component: PorprecoComponent;
  let fixture: ComponentFixture<PorprecoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PorprecoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PorprecoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
