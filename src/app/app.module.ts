import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SlickCarouselModule } from 'ngx-slick-carousel';
// import { HelperService } from './services/helper.services';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './layouts/nav/nav.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { VinhosComponent } from './pages/vinhos/vinhos.component';
import { LojasComponent } from './pages/lojas/lojas.component';
import { EventosComponent } from './pages/eventos/eventos.component';
import { SobreComponent } from './pages/sobre/sobre.component';
import { ContatoComponent } from './pages/contato/contato.component';
import { UnidadesComponent } from './layouts/unidades/unidades.component';
import { MaisvendidosComponent } from './pages/home/maisvendidos/maisvendidos.component';
import { PorprecoComponent } from './pages/home/porpreco/porpreco.component';
import { RegiaoComponent } from './pages/home/regiao/regiao.component';
import { TipoComponent } from './pages/home/tipo/tipo.component';
import { SelecaoComponent } from './pages/home/selecao/selecao.component';
import { MaisvComponent } from './pages/vinhos/maisv/maisv.component';
import { PrecoComponent } from './pages/vinhos/preco/preco.component';
import { PaisComponent } from './pages/vinhos/pais/pais.component';
import { TiposvComponent } from './pages/vinhos/tiposv/tiposv.component';
import { SelecaoespComponent } from './pages/vinhos/selecaoesp/selecaoesp.component';
import { CorporativosComponent } from './pages/eventos/corporativos/corporativos.component';
import { VilleduvinComponent } from './pages/eventos/villeduvin/villeduvin.component';
import { AlphavilleComponent } from './pages/lojas/alphaville/alphaville.component';
import { ItaimComponent } from './pages/lojas/itaim/itaim.component';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    HomeComponent,
    VinhosComponent,
    LojasComponent,
    EventosComponent,
    SobreComponent,
    ContatoComponent,
    UnidadesComponent,
    MaisvendidosComponent,
    PorprecoComponent,
    RegiaoComponent,
    TipoComponent,
    SelecaoComponent,
    MaisvComponent,
    PrecoComponent,
    PaisComponent,
    TiposvComponent,
    SelecaoespComponent,
    CorporativosComponent,
    VilleduvinComponent,
    AlphavilleComponent,
    ItaimComponent
  ],
  imports: [
    BrowserModule,
    SlickCarouselModule,
    AppRoutingModule
  ],
  providers: [] , //HelperService,
  bootstrap: [AppComponent]
})
export class AppModule { }
