import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { VinhosComponent } from './pages/vinhos/vinhos.component';
import { LojasComponent } from './pages/lojas/lojas.component';
import { EventosComponent } from './pages/eventos/eventos.component';
import { SobreComponent } from './pages/sobre/sobre.component';
import { ContatoComponent } from './pages/contato/contato.component';
import { MaisvendidosComponent } from './pages/home/maisvendidos/maisvendidos.component';
import { PorprecoComponent } from './pages/home/porpreco/porpreco.component';
import { RegiaoComponent } from './pages/home/regiao/regiao.component';
import { TipoComponent } from './pages/home/tipo/tipo.component';
import { SelecaoComponent } from './pages/home/selecao/selecao.component';
import { MaisvComponent } from './pages/vinhos/maisv/maisv.component';
import { PaisComponent } from './pages/vinhos/pais/pais.component';
import { PrecoComponent } from './pages/vinhos/preco/preco.component';
import { TiposvComponent } from './pages/vinhos/tiposv/tiposv.component';
import { SelecaoespComponent } from './pages/vinhos/selecaoesp/selecaoesp.component';
import { CorporativosComponent } from './pages/eventos/corporativos/corporativos.component';
import { VilleduvinComponent } from './pages/eventos/villeduvin/villeduvin.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home'},
  { path: 'home', component: HomeComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'maisvendidos' },
      { path: 'maisvendidos', component: MaisvendidosComponent },
      { path: 'porpreco', component: PorprecoComponent },
      { path: 'regiao', component: RegiaoComponent },
      { path: 'tipo', component: TipoComponent },
      { path: 'selecao', component: SelecaoComponent }
    ]
  },
  { path: 'vinhos', component: VinhosComponent },
    { path: 'mais', component: MaisvComponent },
    { path: 'pais', component: PaisComponent },
    { path: 'preco', component: PrecoComponent },
    { path: 'selecaoesp', component: SelecaoespComponent },
    { path: 'tiposv', component: TiposvComponent},
  { path: 'lojas', component: LojasComponent },
  { path: 'eventos', component: EventosComponent },
      { path: 'corporativos', component: CorporativosComponent },
      { path: 'villeduvin', component: VilleduvinComponent },
  { path: 'sobre', component: SobreComponent },
  { path: 'contato', component: ContatoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
